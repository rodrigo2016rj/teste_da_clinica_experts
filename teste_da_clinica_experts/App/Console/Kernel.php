<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel{

  /**
   * Define the application's command schedule.
   */
  protected function schedule(Schedule $schedule): void{
    $schedule->call(function (){
      $update['contador_de_acessos'] = 0;
      DB::table('url')->update($update);
    })->name('zerar_contador_de_acessos')->monthly()->timezone('-0300');
  }

  /**
   * Register the commands for the application.
   */
  protected function commands(): void{
    $this->load(__DIR__.'/Commands');
    require base_path('routes/console.php');
  }

}
