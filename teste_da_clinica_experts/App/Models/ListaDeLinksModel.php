<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\URL;

final class ListaDeLinksModel{

  public function selecionar_urls($filtros, $ordenacao, $quantidade, $descartar){
    $query = DB::table('url');
    $query = $query->addSelect('pk_url');
    $query = $query->addSelect('url_original');
    $query = $query->addSelect('identificador');
    $query = $query->addSelect('url_curta');
    $query = $query->addSelect('contador_de_acessos');

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'url_original':
          $query = $query->where('url_original', 'LIKE', "%$valor%");
          break;
        case 'url_curta':
          $query = $query->where('url_curta', 'LIKE', "%$valor%");
          break;
        case 'identificador':
          $query = $query->where('identificador', 'LIKE', "%$valor%");
          break;
        case 'contador_de_acessos':
          $query = $query->where('contador_de_acessos', '>=', $valor);
          break;
      }
    }

    switch($ordenacao){
      case 'padrao':
        $query = $query->orderBy('pk_url', 'DESC');
        break;
      case 'url_original_em_ordem_alfabetica':
        $query = $query->orderBy('url_original', 'ASC');
        break;
      case 'url_original_em_ordem_alfabetica_inversa':
        $query = $query->orderBy('url_original', 'DESC');
        break;
      case 'url_curta_em_ordem_alfabetica':
        $query = $query->orderBy('url_curta', 'ASC');
        break;
      case 'url_curta_em_ordem_alfabetica_inversa':
        $query = $query->orderBy('url_curta', 'DESC');
        break;
      case 'identificador_em_ordem_alfabetica':
        $query = $query->orderBy('identificador', 'ASC');
        break;
      case 'identificador_em_ordem_alfabetica_inversa':
        $query = $query->orderBy('identificador', 'DESC');
        break;
      case 'contador_de_acessos_em_ordem_crescente':
        $query = $query->orderBy('contador_de_acessos', 'ASC');
        $query = $query->orderBy('pk_url', 'DESC');
        break;
      case 'contador_de_acessos_em_ordem_decrescente':
        $query = $query->orderBy('contador_de_acessos', 'DESC');
        $query = $query->orderBy('pk_url', 'DESC');
        break;
    }

    $query = $query->offset($descartar);
    $query = $query->limit($quantidade);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado = array();
    foreach($array_resultado as $objeto_generico){
      $valores = (array) $objeto_generico;
      $url = new URL($valores);
      $array_melhorado[] = $url;
    }
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

  public function contar_urls($filtros){
    $query = DB::table('url');
    $query = $query->select(DB::raw('COUNT(*) AS quantidade'));

    foreach($filtros as $chave => $valor){
      switch($chave){
        case 'url_original':
          $query = $query->where('url_original', 'LIKE', "%$valor%");
          break;
        case 'url_curta':
          $query = $query->where('url_curta', 'LIKE', "%$valor%");
          break;
        case 'identificador':
          $query = $query->where('identificador', 'LIKE', "%$valor%");
          break;
        case 'contador_de_acessos':
          $query = $query->where('contador_de_acessos', '>=', $valor);
          break;
      }
    }

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    $array_melhorado['quantidade'] = $array_resultado[0]->quantidade;
    $array_resultado = $array_melhorado;

    return $array_resultado;
  }

}
