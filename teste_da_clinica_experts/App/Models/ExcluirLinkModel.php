<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\URL;
use Exception;

final class ExcluirLinkModel{

  public function selecionar_url($id_da_url){
    $query = DB::table('url');
    $query = $query->addSelect('pk_url');
    $query = $query->addSelect('url_original');
    $query = $query->addSelect('identificador');
    $query = $query->addSelect('url_curta');
    $query = $query->addSelect('contador_de_acessos');

    $query = $query->where('pk_url', '=', $id_da_url);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = 'O link não foi encontrado no banco de dados do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];

      $url = new URL($valores);

      $array_melhorado[] = $url;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function excluir_url($id_da_url){
    $array_resultado = array();

    try{
      DB::table('url')->where('pk_url', '=', $id_da_url)->delete();
    }catch(Exception $excecao){
      $array_resultado['mensagem_do_model'] = $excecao->getMessage();
    }

    return $array_resultado;
  }

}
