<?php

namespace App\Models\Entidades;

final class RegistroDeAcesso{
  private $pk_registro_de_acesso;
  private $fk_url;
  private $ip;
  private $user_agent;
  private $momento_do_acesso;
  private $url;

  public function __construct($array_registro_de_acesso = array()){
    if(isset($array_registro_de_acesso['pk_registro_de_acesso'])){
      $this->pk_registro_de_acesso = $array_registro_de_acesso['pk_registro_de_acesso'];
    }
    if(isset($array_registro_de_acesso['fk_url'])){
      $this->fk_url = $array_registro_de_acesso['fk_url'];
    }
    if(isset($array_registro_de_acesso['ip'])){
      $this->ip = $array_registro_de_acesso['ip'];
    }
    if(isset($array_registro_de_acesso['user_agent'])){
      $this->user_agent = $array_registro_de_acesso['user_agent'];
    }
    if(isset($array_registro_de_acesso['momento_do_acesso'])){
      $this->momento_do_acesso = $array_registro_de_acesso['momento_do_acesso'];
    }
    if(isset($array_registro_de_acesso['url'])){
      $this->url = $array_registro_de_acesso['url'];
    }
  }

  public function set_pk_registro_de_acesso($pk_registro_de_acesso){
    $this->pk_registro_de_acesso = $pk_registro_de_acesso;
  }

  public function set_fk_url($fk_url){
    $this->fk_url = $fk_url;
  }

  public function set_ip($ip){
    $this->ip = $ip;
  }

  public function set_user_agent($user_agent){
    $this->user_agent = $user_agent;
  }

  public function set_momento_do_acesso($momento_do_acesso){
    $this->momento_do_acesso = $momento_do_acesso;
  }

  public function set_url($url){
    $this->url = $url;
  }

  public function get_pk_registro_de_acesso(){
    return $this->pk_registro_de_acesso;
  }

  public function get_fk_url(){
    return $this->fk_url;
  }

  public function get_ip(){
    return $this->ip;
  }

  public function get_user_agent(){
    return $this->user_agent;
  }

  public function get_momento_do_acesso(){
    return $this->momento_do_acesso;
  }

  public function get_url(){
    return $this->url;
  }

}
