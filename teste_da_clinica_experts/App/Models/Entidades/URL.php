<?php

namespace App\Models\Entidades;

final class URL{
  private $pk_url;
  private $url_original;
  private $identificador;
  private $url_curta;
  private $contador_de_acessos;

  public function __construct($array_url = array()){
    if(isset($array_url['pk_url'])){
      $this->pk_url = $array_url['pk_url'];
    }
    if(isset($array_url['url_original'])){
      $this->url_original = $array_url['url_original'];
    }
    if(isset($array_url['identificador'])){
      $this->identificador = $array_url['identificador'];
    }
    if(isset($array_url['url_curta'])){
      $this->url_curta = $array_url['url_curta'];
    }
    if(isset($array_url['contador_de_acessos'])){
      $this->contador_de_acessos = $array_url['contador_de_acessos'];
    }
  }

  public function set_pk_url($pk_url){
    $this->pk_url = $pk_url;
  }

  public function set_url_original($url_original){
    $this->url_original = $url_original;
  }

  public function set_identificador($identificador){
    $this->identificador = $identificador;
  }

  public function set_url_curta($url_curta){
    $this->url_curta = $url_curta;
  }

  public function set_contador_de_acessos($contador_de_acessos){
    $this->contador_de_acessos = $contador_de_acessos;
  }

  public function get_pk_url(){
    return $this->pk_url;
  }

  public function get_url_original(){
    return $this->url_original;
  }

  public function get_identificador(){
    return $this->identificador;
  }

  public function get_url_curta(){
    return $this->url_curta;
  }

  public function get_contador_de_acessos(){
    return $this->contador_de_acessos;
  }

  public function caracteres_permitidos_para_identificador($somente_letras_e_numeros = false){
    $caracteres_permitidos = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $caracteres_permitidos .= '0123456789';
    if(!$somente_letras_e_numeros){
      $caracteres_permitidos .= '_.-~';
    }
    return $caracteres_permitidos;
  }

  public function quantidade_minima_de_caracteres($atributo){
    switch($atributo){
      case 'url_original':
        return 4;
      case 'identificador':
        return 2;
    }
    return -1;
  }

  //O método abaixo deve ser sempre igual ou mais restritivo que o banco de dados
  public function quantidade_maxima_de_caracteres($atributo){
    switch($atributo){
      case 'url_original':
        return 2000;
      case 'identificador':
        return 20;
    }
    return -1;
  }

}
