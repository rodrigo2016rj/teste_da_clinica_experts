<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\URL;
use Exception;

final class RedirecionarModel{

  public function selecionar_url($identificador){
    $query = DB::table('url');
    $query = $query->addSelect('pk_url');
    $query = $query->addSelect('url_original');
    $query = $query->addSelect('identificador');

    $query = $query->where('identificador', '=', $identificador);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = "Nenhuma url com o identificador $identificador foi encontrada no banco";
      $mensagem_do_model .= ' de dados deste sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];

      $url = new URL();
      $url->set_pk_url($valores['pk_url']);
      $url->set_url_original($valores['url_original']);
      $url->set_identificador($valores['identificador']);

      $array_melhorado[] = $url;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function cadastrar_registro_de_acesso($registro_de_acesso){
    $insert['fk_url'] = $registro_de_acesso->get_fk_url();
    $insert['ip'] = $registro_de_acesso->get_ip();
    $insert['user_agent'] = $registro_de_acesso->get_user_agent();
    $insert['momento_do_acesso'] = $registro_de_acesso->get_momento_do_acesso();

    $array_resultado = array();

    try{
      DB::table('registro_de_acesso')->insert($insert);
    }catch(Exception $excecao){
      $array_resultado['mensagem_do_model'] = $excecao->getMessage();
    }

    return $array_resultado;
  }

  public function aumentar_contador_de_acessos($pk_url){
    $array_resultado = array();

    try{
      DB::table('url')->where('pk_url', '=', $pk_url)->increment('contador_de_acessos', 1);
    }catch(Exception $excecao){
      $array_resultado['mensagem_do_model'] = $excecao->getMessage();
    }

    return $array_resultado;
  }

}
