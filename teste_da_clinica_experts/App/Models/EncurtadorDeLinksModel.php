<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use PDOException;

final class EncurtadorDeLinksModel{

  public function verificar_disponibilidade_de_identificador($identificador){
    $query = DB::table('url');
    $query = $query->addSelect('pk_url');

    $query = $query->where('identificador', $identificador);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O identificador informado não está disponível.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function cadastrar_url($url){
    $insert['url_original'] = $url->get_url_original();
    $insert['identificador'] = $url->get_identificador();
    $insert['url_curta'] = $url->get_url_curta();

    $array_resultado = array();

    try{
      DB::table('url')->insert($insert);
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        case 1062:
          $mensagem = 'Já existe uma url cadastrada com uma ou mais destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
