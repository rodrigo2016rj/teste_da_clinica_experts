<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Models\Entidades\URL;
use PDOException;

final class EditarLinkModel{

  public function selecionar_url($id_da_url){
    $query = DB::table('url');
    $query = $query->addSelect('pk_url');
    $query = $query->addSelect('url_original');
    $query = $query->addSelect('identificador');
    $query = $query->addSelect('url_curta');
    $query = $query->addSelect('contador_de_acessos');

    $query = $query->where('pk_url', '=', $id_da_url);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) === 0){
      $mensagem_do_model = 'O link não foi encontrado no banco de dados do sistema.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }else{
      $valores = (array) $array_resultado[0];

      $url = new URL($valores);

      $array_melhorado[] = $url;
      $array_resultado = $array_melhorado;
    }

    return $array_resultado;
  }

  public function verificar_disponibilidade_de_identificador($identificador, $id_da_url){
    $query = DB::table('url');
    $query = $query->addSelect('pk_url');

    $query = $query->where('identificador', $identificador);
    $query = $query->where('pk_url', '<>', $id_da_url);

    $colecao = $query->get();
    $array_resultado = $colecao->all();

    if(count($array_resultado) > 0){
      $mensagem_do_model = 'O identificador informado não está disponível.';
      $array_resultado['mensagem_do_model'] = $mensagem_do_model;
    }

    return $array_resultado;
  }

  public function editar_url($url){
    $update['identificador'] = $url->get_identificador();
    $update['url_curta'] = $url->get_url_curta();

    $array_resultado = array();

    try{
      DB::table('url')->where('pk_url', '=', $url->get_pk_url())->update($update);
    }catch(PDOException $excecao){
      $codigo_da_excecao = $excecao->errorInfo[1];
      switch($codigo_da_excecao){
        case 1062:
          $mensagem = 'Já existe uma url cadastrada com uma ou mais destas informações.';
          $array_resultado['mensagem_do_model'] = $mensagem;
          break;
        default:
          $array_resultado['mensagem_do_model'] = $excecao->getMessage();
          break;
      }
    }

    return $array_resultado;
  }

}
