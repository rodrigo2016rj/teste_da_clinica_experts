<?php

namespace App\Http\Controllers;

use App\Models\RedirecionarModel;
use App\Models\Entidades\RegistroDeAcesso;
use DateTimeZone;
use DateTime;

final class RedirecionarController extends TemplateLayoutController{

  public function redirecionar($identificador){
    $redirecionar_model = new RedirecionarModel();

    /* Ajeitando valores recebidos */
    $identificador = trim($identificador ?? '');

    /* Consultando url */
    $array_resultado = $redirecionar_model->selecionar_url($identificador);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'Erro 404.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      echo $mensagem;
      die;
    }
    $url = $array_resultado[0];

    /* Criando o registro de acesso */
    $registro_de_acesso = new RegistroDeAcesso();

    $id_da_url = $url->get_pk_url();
    $registro_de_acesso->set_fk_url($id_da_url);

    $ip = $_SERVER['REMOTE_ADDR'];
    $registro_de_acesso->set_ip($ip);

    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $registro_de_acesso->set_user_agent($user_agent);

    $sem_fuso_horario = new DateTimeZone('GMT');
    $objeto_date_time = new DateTime('now', $sem_fuso_horario);
    $momento_atual = $objeto_date_time->format('Y-m-d H:i:s');
    $registro_de_acesso->set_momento_do_acesso($momento_atual);

    /* Cadastrando o registro de acesso */
    $redirecionar_model->cadastrar_registro_de_acesso($registro_de_acesso);

    /* Aumentando o contador de acessos */
    $redirecionar_model->aumentar_contador_de_acessos($id_da_url);

    /* Se for redirecionar para este sistema, redireciona para a página encurtador_de_links */
    $url_original = $url->get_url_original();
    $url_interna = rtrim(config('app.url'), ':0123456789');
    $url_interna = str_replace('https', 'http', $url_interna);
    $url_externa = $url_original;
    $url_externa = str_replace('https', 'http', $url_externa);
    if(str_contains($url_externa, $url_interna)){
      header('Location: /encurtador_de_links');
      die;
    }

    /* Redirecionando para a url original */
    header("Location: $url_original");
    die;
  }

}
