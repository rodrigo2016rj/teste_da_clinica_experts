<?php

namespace App\Http\Controllers;

use App\Models\EditarLinkModel;
use App\Models\Entidades\URL;
use Inertia\Inertia;

final class EditarLinkController extends TemplateLayoutController{

  public function carregar_pagina($id_do_link){
    $valores = $this->valores_do_template_layout();

    $editar_link_model = new EditarLinkModel();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['editar_link']['mensagem_de_falha'] = '';
    $valores['editar_link']['mensagem_de_sucesso'] = '';
    $valores['editar_link']['link']['id_valido'] = true;
    $valores['editar_link']['link']['id_do_link'] = '';
    $valores['editar_link']['link']['url_original'] = '';
    $valores['editar_link']['link']['url_curta'] = '';
    $valores['editar_link']['link']['contador_de_acessos'] = '';
    $valores['editar_link']['link']['identificador'] = '';

    /* Validando o ID do link informado na URL */
    if(!is_numeric($id_do_link) or $id_do_link <= 0 or floor($id_do_link) != $id_do_link){
      $mensagem = 'O endereço digitado não está correto, o endereço precisa ter um número natural';
      $mensagem .= ' maior que zero.';
      $valores['editar_link']['mensagem_de_falha'] = $mensagem;
      $valores['editar_link']['link']['id_valido'] = false;
    }else{
      /* Consultando e mostrando informações do link */
      $array_resultado = $editar_link_model->selecionar_url($id_do_link);
      if(isset($array_resultado['mensagem_do_model'])){
        $valores['editar_link']['mensagem_de_falha'] = $array_resultado['mensagem_do_model'];
        $valores['editar_link']['link']['id_valido'] = false;
      }else{
        $url = $array_resultado[0];

        $id_da_url = $url->get_pk_url();
        $valores['editar_link']['link']['id_do_link'] = $id_da_url;

        $url_original = $url->get_url_original();
        $valores['editar_link']['link']['url_original'] = $url_original;

        $url_curta = $url->get_url_curta();
        $valores['editar_link']['link']['url_curta'] = $url_curta;

        $contador_de_acessos = $url->get_contador_de_acessos();
        $valores['editar_link']['link']['contador_de_acessos'] = $contador_de_acessos;

        $identificador = $url->get_identificador();
        $valores['editar_link']['link']['identificador'] = $identificador;
      }
    }

    return Inertia::render('editar_link/editar_link', $valores);
  }

  public function servico_put_editar_link(){
    $retorno = $this->editar_link();
    return response($retorno, 200);
  }

  private function editar_link(){
    $editar_link_model = new EditarLinkModel();
    $url = new URL();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $id_do_link = $requisicao->post('id_do_link');
    $identificador = trim($requisicao->post('identificador') ?? '');

    /* Validando o ID do link */
    if(!is_numeric($id_do_link) or $id_do_link <= 0 or floor($id_do_link) != $id_do_link){
      $mensagem = 'O link não foi editado.';
      $mensagem .= ' O ID do link precisa ser um número natural maior que zero.';
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $array_resultado = $editar_link_model->selecionar_url($id_do_link);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O link não foi editado.';
      $mensagem .= " {$array_resultado['mensagem_do_model']}";
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $url = $array_resultado[0];
    $id_da_url = $url->get_pk_url();

    /* Validando o identificador */
    if($identificador == ''){
      $mensagem = 'O link não foi editado.';
      $mensagem .= ' O identificador não pode ficar em branco.';
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $array_caracteres_do_identificador = mb_str_split($identificador, 1);
    $caracteres_permitidos = $url->caracteres_permitidos_para_identificador();
    foreach($array_caracteres_do_identificador as $caractere){
      if(strpos($caracteres_permitidos, $caractere) === false){
        if($caractere === ' '){
          $caractere = 'espaço';
        }
        $mensagem = 'O link não foi editado.';
        $mensagem .= ' O valor escolhido para o identificador não é válido.';
        $mensagem .= " O identificador não pode ter o caractere $caractere.";
        $mensagem .= ' O identificador só pode ter os seguintes caracteres:';
        $caracteres_permitidos = implode(' ', (str_split($caracteres_permitidos, 1)));
        $mensagem .= " $caracteres_permitidos";
        $valores['mensagem_de_falha'] = $mensagem;
        return $valores;
      }
    }
    $array_resultado = $editar_link_model->verificar_disponibilidade_de_identificador($identificador,
      $id_da_url);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O link não foi editado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $minimo = $url->quantidade_minima_de_caracteres('identificador');
    $maximo = $url->quantidade_maxima_de_caracteres('identificador');
    $quantidade = mb_strlen($identificador);
    if($quantidade < $minimo){
      $mensagem = 'O link não foi editado.';
      $mensagem .= " O identificador precisa ter no mínimo $minimo caracteres.";
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    if($quantidade > $maximo){
      $mensagem = 'O link não foi editado.';
      $mensagem .= " O identificador não pode ultrapassar $maximo caracteres.";
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $url->set_identificador($identificador);

    if(str_ends_with(config('app.url'), '/')){
      $url_curta = config('app.url').'r/'.$identificador;
    }else{
      $url_curta = config('app.url').'/r/'.$identificador;
    }
    $url->set_url_curta($url_curta);

    /* Editando url no banco de dados */
    $array_resultado = $editar_link_model->editar_url($url);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O link não foi editado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }

    /* Retornando o resultado */
    $mensagem = 'O link foi editado com sucesso.';
    $valores['mensagem_de_sucesso'] = $mensagem;
    $valores['link']['id_do_link'] = $url->get_pk_url();
    $valores['link']['url_original'] = $url->get_url_original();
    $valores['link']['url_curta'] = $url->get_url_curta();
    $valores['link']['contador_de_acessos'] = $url->get_contador_de_acessos();
    $valores['link']['identificador'] = $url->get_identificador();
    return $valores;
  }

}
