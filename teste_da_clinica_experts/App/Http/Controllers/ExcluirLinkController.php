<?php

namespace App\Http\Controllers;

use App\Models\ExcluirLinkModel;
use App\Models\Entidades\URL;
use Inertia\Inertia;

final class ExcluirLinkController extends TemplateLayoutController{

  public function carregar_pagina($id_do_link){
    $valores = $this->valores_do_template_layout();

    $excluir_link_model = new ExcluirLinkModel();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['excluir_link']['mensagem_de_falha'] = '';
    $valores['excluir_link']['mensagem_de_sucesso'] = '';
    $valores['excluir_link']['link']['id_valido'] = true;
    $valores['excluir_link']['link']['id_do_link'] = '';
    $valores['excluir_link']['link']['url_original'] = '';
    $valores['excluir_link']['link']['url_curta'] = '';
    $valores['excluir_link']['link']['identificador'] = '';
    $valores['excluir_link']['link']['contador_de_acessos'] = '';

    /* Validando o ID do link informado na URL */
    if(!is_numeric($id_do_link) or $id_do_link <= 0 or floor($id_do_link) != $id_do_link){
      $mensagem = 'O endereço digitado não está correto, o endereço precisa ter um número natural';
      $mensagem .= ' maior que zero.';
      $valores['excluir_link']['mensagem_de_falha'] = $mensagem;
      $valores['excluir_link']['link']['id_valido'] = false;
    }else{
      /* Consultando e mostrando informações do link */
      $array_resultado = $excluir_link_model->selecionar_url($id_do_link);
      if(isset($array_resultado['mensagem_do_model'])){
        $valores['excluir_link']['mensagem_de_falha'] = $array_resultado['mensagem_do_model'];
        $valores['excluir_link']['link']['id_valido'] = false;
      }else{
        $url = $array_resultado[0];

        $id_da_url = $url->get_pk_url();
        $valores['excluir_link']['link']['id_do_link'] = $id_da_url;

        $url_original = $url->get_url_original();
        $valores['excluir_link']['link']['url_original'] = $url_original;

        $url_curta = $url->get_url_curta();
        $valores['excluir_link']['link']['url_curta'] = $url_curta;

        $contador_de_acessos = $url->get_contador_de_acessos();
        $valores['excluir_link']['link']['contador_de_acessos'] = $contador_de_acessos;

        $identificador = $url->get_identificador();
        $valores['excluir_link']['link']['identificador'] = $identificador;
      }
    }

    return Inertia::render('excluir_link/excluir_link', $valores);
  }

  public function servico_delete_excluir_link(){
    $retorno = $this->excluir_link();
    return response($retorno, 200);
  }

  private function excluir_link(){
    $excluir_link_model = new ExcluirLinkModel();
    $url = new URL();

    /* Obtendo valores do formulário */
    $requisicao = $this->get_requisicao();
    $id_do_link = $requisicao->post('id_do_link');

    /* Validando o ID do link */
    if(!is_numeric($id_do_link) or $id_do_link <= 0 or floor($id_do_link) != $id_do_link){
      $mensagem = 'O link não foi excluído.';
      $mensagem .= ' O ID do link precisa ser um número natural maior que zero.';
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $array_resultado = $excluir_link_model->selecionar_url($id_do_link);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = $array_resultado['mensagem_do_model'];
      $mensagem .= ' O link pode já ter sido excluído.';
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }

    /* Excluindo url no banco de dados */
    $array_resultado = $excluir_link_model->excluir_url($id_do_link);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O link não foi excluído.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }

    /* Retornando o resultado */
    $mensagem = 'O link foi excluído com sucesso.';
    $valores['mensagem_de_sucesso'] = $mensagem;
    return $valores;
  }

}
