<?php

namespace App\Http\Controllers;

use App\Models\ListaDeLinksModel;
use Inertia\Inertia;

final class ListaDeLinksController extends TemplateLayoutController{
  private const QUANTIDADE_PADRAO_POR_PAGINA = 15;

  public function carregar_pagina(){
    $valores = $this->valores_do_template_layout();

    /* Colocando valores iniciais nas variáveis para não ficarem undefined no Vue */
    $valores['lista_de_links']['quantidades_por_pagina'] = $this->criar_array_quantidades_por_pagina();
    $valores['lista_de_links']['lista_de_links'] = $this->retornar_lista_de_links();

    return Inertia::render('lista_de_links/lista_de_links', $valores);
  }

  public function servico_get_lista_de_links(){
    $retorno['lista_de_links'] = $this->retornar_lista_de_links();
    return response($retorno, 200);
  }

  private function criar_array_quantidades_por_pagina(){
    $quantidades_por_pagina['1'] = '1';
    $quantidades_por_pagina['5'] = '5';
    $quantidades_por_pagina['10'] = '10';
    $quantidades_por_pagina['15'] = '15';
    $quantidades_por_pagina['20'] = '20';
    $quantidades_por_pagina['25'] = '25';
    $quantidades_por_pagina['30'] = '30';
    $quantidades_por_pagina['40'] = '40';
    $quantidades_por_pagina['50'] = '50';
    $quantidades_por_pagina['60'] = '60';
    $quantidades_por_pagina['100'] = '100';
    $quantidades_por_pagina['120'] = '120';

    return $quantidades_por_pagina;
  }

  private function retornar_lista_de_links(){
    $lista_de_links_model = new ListaDeLinksModel();

    $valores = array();

    $requisicao = $this->get_requisicao();

    /* Preparando os filtros */
    $filtros = array();
    $filtro_url_original = trim($requisicao->get('filtro_url_original') ?? '');
    if($filtro_url_original !== ''){
      $filtros['url_original'] = $filtro_url_original;
    }
    $valores['filtro_url_original'] = $filtro_url_original;

    $filtro_url_curta = trim($requisicao->get('filtro_url_curta') ?? '');
    if($filtro_url_curta !== ''){
      $filtros['url_curta'] = $filtro_url_curta;
    }
    $valores['filtro_url_curta'] = $filtro_url_curta;

    $filtro_identificador = trim($requisicao->get('filtro_identificador') ?? '');
    if($filtro_identificador !== ''){
      $filtros['identificador'] = $filtro_identificador;
    }
    $valores['filtro_identificador'] = $filtro_identificador;

    $filtro_contador_de_acessos = $requisicao->get('filtro_contador_de_acessos') ?? '';
    if($filtro_contador_de_acessos !== ''){
      $filtros['contador_de_acessos'] = $filtro_contador_de_acessos;
    }
    $valores['filtro_contador_de_acessos'] = $filtro_contador_de_acessos;

    /* Preparando a ordenação */
    $ordenacao = $requisicao->get('ordenacao');
    $valores['ordem_da_url_original'] = '';
    $valores['ordem_da_url_curta'] = '';
    $valores['ordem_do_identificador'] = '';
    $valores['ordem_do_contador_de_acessos'] = '';
    switch($ordenacao){
      case 'padrao':
        break;
      case 'url_original_em_ordem_alfabetica':
        $valores['ordem_da_url_original'] = ' (A → Z)';
        break;
      case 'url_original_em_ordem_alfabetica_inversa':
        $valores['ordem_da_url_original'] = ' (Z → A)';
        break;
      case 'url_curta_em_ordem_alfabetica':
        $valores['ordem_da_url_curta'] = ' (A → Z)';
        break;
      case 'url_curta_em_ordem_alfabetica_inversa':
        $valores['ordem_da_url_curta'] = ' (Z → A)';
        break;
      case 'identificador_em_ordem_alfabetica':
        $valores['ordem_do_identificador'] = ' (A → Z)';
        break;
      case 'identificador_em_ordem_alfabetica_inversa':
        $valores['ordem_do_identificador'] = ' (Z → A)';
        break;
      case 'contador_de_acessos_em_ordem_crescente':
        $valores['ordem_do_contador_de_acessos'] = ' (0 → 9)';
        break;
      case 'contador_de_acessos_em_ordem_decrescente':
        $valores['ordem_do_contador_de_acessos'] = ' (9 → 0)';
        break;
      default:
        $ordenacao = 'padrao';
        break;
    }
    $valores['ordenacao'] = $ordenacao;

    /* Preparando a paginação */
    $quantidade_por_pagina = (int) $requisicao->get('quantidade_por_pagina');
    $quantidades_por_pagina = $this->criar_array_quantidades_por_pagina();
    if(in_array($quantidade_por_pagina, $quantidades_por_pagina)){
      $valores['quantidade_por_pagina'] = $quantidade_por_pagina;
    }else{
      $quantidade_por_pagina = self::QUANTIDADE_PADRAO_POR_PAGINA;
      $valores['quantidade_por_pagina'] = 'padrao';
    }

    $pagina = (int) $requisicao->get('pagina');
    if($pagina < 1){
      $pagina = 1;
    }
    $quantidade_de_paginas = $this->calcular_quantidade_de_paginas_da_lista_de_links($filtros,
      $quantidade_por_pagina);
    if($pagina > $quantidade_de_paginas){
      $pagina = $quantidade_de_paginas;
    }

    $valores['pagina_atual'] = $pagina;
    $valores['ultima_pagina'] = $quantidade_de_paginas;

    $descartar = $quantidade_por_pagina * $pagina - $quantidade_por_pagina;
    $descartar = $descartar >= 0 ? $descartar : 0;

    /* Preparando o resultado */
    $links = $lista_de_links_model->selecionar_urls($filtros, $ordenacao, $quantidade_por_pagina,
      $descartar);
    $array_links = array();
    foreach($links as $link){
      $array_link = array();

      $id_da_url = $link->get_pk_url();
      $array_link['id_da_url'] = $id_da_url;

      $url_original = $link->get_url_original();
      $array_link['url_original'] = $url_original;

      $url_curta = $link->get_url_curta();
      $array_link['url_curta'] = $url_curta;

      $identificador = $link->get_identificador();
      $array_link['identificador'] = $identificador;

      $contador_de_acessos = $link->get_contador_de_acessos();
      $array_link['contador_de_acessos'] = $contador_de_acessos;

      $array_links[] = $array_link;
    }

    $valores['links'] = $array_links;

    return $valores;
  }

  private function calcular_quantidade_de_paginas_da_lista_de_links($filtros, $quantidade_por_pagina){
    $lista_de_links_model = new ListaDeLinksModel();

    $array_resultado = $lista_de_links_model->contar_urls($filtros);
    $quantidade_de_paginas = ceil($array_resultado['quantidade'] / $quantidade_por_pagina);

    return $quantidade_de_paginas;
  }

}
