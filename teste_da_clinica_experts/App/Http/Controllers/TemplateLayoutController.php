<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class TemplateLayoutController extends Controller{
  /* Armazena o objeto da requisição. */
  private $requisicao;

  public function __construct(Request $requisicao){
    $this->requisicao = $requisicao;
  }

  protected final function get_requisicao(){
    return $this->requisicao;
  }

  /** ---------------------------------------------------------------------------------------------
    Retorna valores que podem ser usados em várias páginas deste sistema */
  protected final function valores_do_template_layout(){
    $valores = array();

    $valores['template_layout']['visual_escolhido'] = 'visual_padrao';

    return $valores;
  }

}
