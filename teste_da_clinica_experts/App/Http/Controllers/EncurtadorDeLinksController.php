<?php

namespace App\Http\Controllers;

use App\Models\EncurtadorDeLinksModel;
use App\Models\Entidades\URL;
use Inertia\Inertia;

final class EncurtadorDeLinksController extends TemplateLayoutController{

  public function carregar_pagina(){
    $valores = $this->valores_do_template_layout();
    return Inertia::render('encurtador_de_links/encurtador_de_links', $valores);
  }

  public function servico_post_encurtar_url(){
    $retorno = $this->encurtar_url();
    return response($retorno, 200);
  }

  private function encurtar_url(){
    $encurtador_de_links_model = new EncurtadorDeLinksModel();
    $url = new URL();

    /* Obtendo valores da requisição */
    $requisicao = $this->get_requisicao();
    $url_original = trim($requisicao->post('url_original') ?? '');
    $identificador = trim($requisicao->post('identificador') ?? '');

    /* Validando a url original */
    if($url_original === ''){
      $mensagem = 'O link não foi encurtado.';
      $mensagem .= ' O endereço do link (url) precisa ser preenchido.';
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $minimo = $url->quantidade_minima_de_caracteres('url_original');
    $maximo = $url->quantidade_maxima_de_caracteres('url_original');
    $quantidade = mb_strlen($url_original);
    if($quantidade < $minimo){
      $mensagem = 'O link não foi encurtado.';
      $mensagem .= " O endereço do link (url) precisa ter no mínimo $minimo caracteres.";
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    if($quantidade > $maximo){
      $mensagem = 'O link não foi encurtado.';
      $mensagem .= " O endereço do link (url) não pode ultrapassar $maximo caracteres.";
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    if(!filter_var($url_original, FILTER_VALIDATE_URL)){
      $mensagem = 'O link não foi encurtado.';
      $mensagem .= ' O endereço do link (url) digitado não foi considerado como sendo uma url válida.';
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }
    $url->set_url_original($url_original);

    /* Validando o identificador */
    if($identificador !== ''){
      $array_caracteres_do_identificador = mb_str_split($identificador, 1);
      $caracteres_permitidos = $url->caracteres_permitidos_para_identificador();
      foreach($array_caracteres_do_identificador as $caractere){
        if(strpos($caracteres_permitidos, $caractere) === false){
          if($caractere === ' '){
            $caractere = 'espaço';
          }
          $mensagem = 'O link não foi encurtado.';
          $mensagem .= ' O valor escolhido para o identificador não é válido.';
          $mensagem .= " O identificador não pode ter o caractere $caractere.";
          $mensagem .= ' O identificador só pode ter os seguintes caracteres:';
          $caracteres_permitidos = implode(' ', (str_split($caracteres_permitidos, 1)));
          $mensagem .= " $caracteres_permitidos";
          $valores['mensagem_de_falha'] = $mensagem;
          return $valores;
        }
      }
      $array_resultado = $encurtador_de_links_model->verificar_disponibilidade_de_identificador($identificador);
      if(isset($array_resultado['mensagem_do_model'])){
        $mensagem = 'O link não foi encurtado.';
        $mensagem .= ' '.$array_resultado['mensagem_do_model'];
        $valores['mensagem_de_falha'] = $mensagem;
        return $valores;
      }
      $minimo = $url->quantidade_minima_de_caracteres('identificador');
      $maximo = $url->quantidade_maxima_de_caracteres('identificador');
      $quantidade = mb_strlen($identificador);
      if($quantidade < $minimo){
        $mensagem = 'O link não foi encurtado.';
        $mensagem .= " O identificador precisa ter no mínimo $minimo caracteres.";
        $valores['mensagem_de_falha'] = $mensagem;
        return $valores;
      }
      if($quantidade > $maximo){
        $mensagem = 'O link não foi encurtado.';
        $mensagem .= " O identificador não pode ultrapassar $maximo caracteres.";
        $valores['mensagem_de_falha'] = $mensagem;
        return $valores;
      }
    }else{
      /* Definindo o identificador por meio de uma string gerada aleatoriamente. */
      $caracteres_permitidos = $url->caracteres_permitidos_para_identificador(true);
      $primeira_posicao = 0;
      $ultima_posicao = strlen($caracteres_permitidos) - 1;
      do{
        $identificador = '';
        $quantidade_de_caracteres = random_int(6, 8); //De 6 a 8 caracteres.
        for($i = 1; $i <= $quantidade_de_caracteres; $i++){
          $posicao_sorteada = random_int($primeira_posicao, $ultima_posicao);
          $identificador .= substr($caracteres_permitidos, $posicao_sorteada, 1);
        }
        $array_resultado = $encurtador_de_links_model->verificar_disponibilidade_de_identificador($identificador);
      }while(isset($array_resultado['mensagem_do_model'])); //Se não estiver disponível, define outro.
    }
    $url->set_identificador($identificador);

    /* Criando url curta */
    if(str_ends_with(config('app.url'), '/')){
      $url_curta = config('app.url').'r/'.$identificador;
    }else{
      $url_curta = config('app.url').'/r/'.$identificador;
    }
    $url->set_url_curta($url_curta);

    /* Cadastrando url no banco de dados */
    $array_resultado = $encurtador_de_links_model->cadastrar_url($url);
    if(isset($array_resultado['mensagem_do_model'])){
      $mensagem = 'O link não foi encurtado.';
      $mensagem .= ' '.$array_resultado['mensagem_do_model'];
      $valores['mensagem_de_falha'] = $mensagem;
      return $valores;
    }

    /* Retornando o resultado */
    $mensagem = 'O link foi encurtado com sucesso.';
    $valores['mensagem_de_sucesso'] = $mensagem;
    $valores['url_curta'] = $url_curta;
    return $valores;
  }

}
