<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EncurtadorDeLinksController;
use App\Http\Controllers\ListaDeLinksController;
use App\Http\Controllers\EditarLinkController;
use App\Http\Controllers\ExcluirLinkController;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider and all of them will
  | be assigned to the "api" middleware group. Make something great!
  |
 */

/* Serviços da Página Encurtador de Links */
Route::post('encurtador_de_links/servico_post_encurtar_url', [EncurtadorDeLinksController::class, 'servico_post_encurtar_url']);

/* Serviços da Página Lista de Links */
Route::get('lista_de_links/servico_get_lista_de_links', [ListaDeLinksController::class, 'servico_get_lista_de_links']);

/* Serviços da Página Editar Link */
Route::put('editar_link/servico_put_editar_link', [EditarLinkController::class, 'servico_put_editar_link']);

/* Serviços da Página Excluir Link */
Route::delete('excluir_link/servico_delete_excluir_link', [ExcluirLinkController::class, 'servico_delete_excluir_link']);
