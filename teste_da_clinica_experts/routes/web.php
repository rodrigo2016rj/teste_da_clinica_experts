<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EncurtadorDeLinksController;
use App\Http\Controllers\RedirecionarController;
use App\Http\Controllers\ListaDeLinksController;
use App\Http\Controllers\EditarLinkController;
use App\Http\Controllers\ExcluirLinkController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider and all of them will
  | be assigned to the "web" middleware group. Make something great!
  |
 */

/* Página Padrão */
Route::get('/', [EncurtadorDeLinksController::class, 'carregar_pagina']);

/* Página Encurtador de Links */
Route::get('/encurtador_de_links', [EncurtadorDeLinksController::class, 'carregar_pagina']);

/* Redirecionar */
Route::get('/r/{identificador}', [RedirecionarController::class, 'redirecionar']);

/* Página Lista de Links */
Route::get('/lista_de_links', [ListaDeLinksController::class, 'carregar_pagina']);

/* Página Editar Link */
Route::get('/editar_link/{id_do_link}', [EditarLinkController::class, 'carregar_pagina']);

/* Página Excluir Link */
Route::get('/excluir_link/{id_do_link}', [ExcluirLinkController::class, 'carregar_pagina']);
