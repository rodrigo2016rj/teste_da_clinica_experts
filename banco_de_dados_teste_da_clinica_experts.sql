-- -------------------------------------------------------------------------
-- banco_de_dados_teste_da_clinica_experts:

DROP SCHEMA IF EXISTS banco_de_dados_teste_da_clinica_experts;

CREATE SCHEMA IF NOT EXISTS banco_de_dados_teste_da_clinica_experts 
DEFAULT CHARACTER SET utf8mb4 
COLLATE utf8mb4_unicode_ci;

USE banco_de_dados_teste_da_clinica_experts;

-- -------------------------------------------------------------------------
-- Tabela url:

DROP TABLE IF EXISTS url;

CREATE TABLE IF NOT EXISTS url(
  pk_url INT NOT NULL AUTO_INCREMENT,
  url_original VARCHAR(2000) NOT NULL,
  identificador VARCHAR(20) NOT NULL,
  url_curta VARCHAR(400) NOT NULL,
  contador_de_acessos INT UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (pk_url),
  UNIQUE INDEX identificador_UNICA (identificador ASC),
  UNIQUE INDEX url_curta_UNICA (url_curta ASC)
)
ENGINE = InnoDB;

-- -------------------------------------------------------------------------
-- Tabela registro_de_acesso:

DROP TABLE IF EXISTS registro_de_acesso;

CREATE TABLE IF NOT EXISTS registro_de_acesso(
  pk_registro_de_acesso INT NOT NULL AUTO_INCREMENT,
  fk_url INT NOT NULL,
  ip VARCHAR(45) NOT NULL,
  user_agent VARCHAR(255) NOT NULL,
  momento_do_acesso DATETIME NOT NULL,
  PRIMARY KEY (pk_registro_de_acesso),
  INDEX fk_url_INDICE (fk_url ASC),
  CONSTRAINT fk_url_tabela_registro_de_acesso
    FOREIGN KEY (fk_url)
    REFERENCES url (pk_url)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)
ENGINE = InnoDB;
